#!/bin/bash
# download and place into system bin
cd /home/ubuntu
wget -O - https://github.com/prometheus/node_exporter/releases/download/v1.3.1/node_exporter-1.3.1.linux-amd64.tar.gz | tar xzf -
sudo mv /home/ubuntu/node_exporter-1.3.1.linux-amd64/node_exporter /usr/local/bin/

# setup service
sudo bash
cat << EOF > /etc/systemd/system/node-exporter.service
[Unit]
Description=Prometheus Node Exporter

[Service]
Restart=always
ExecStart=/usr/local/bin/node_exporter

[Install]
WantedBy=multi-user.target
EOF
exit
sudo systemctl daemon-reload
sudo systemctl enable node-exporter
sudo systemctl start node-exporter

# clean up
rm -r /home/ubuntu/node_exporter-1.3.1.linux-amd64
